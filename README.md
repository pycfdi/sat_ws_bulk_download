# pyCFDI/credentials
:us: The documentation of this project is in spanish as this is the natural language for intended audience.

:es: La documentación del proyecto está en español porque ese es el lenguaje principal de los usuarios.

Este proyecto está inspirado en [phpcfdi/sat-ws-descarga-masiva](https://github.com/phpcfdi/sat-ws-descarga-masiva)

## Descripción
Esta librería ha sido creada para poder trabajar con el Web Service de Descarga Masiva de CFDI's (XML) y Metadata.

Ofrece posibilidades para la creación y envio de nuevas peticiones de descarga, indicando el tipo de descarga (Emitidos o Recibidos), el tipo de petición (CFDI o Metadata) , ventana de tiempo de la solicitud y demás argumentos disponibles en las peticiones del SAT.
