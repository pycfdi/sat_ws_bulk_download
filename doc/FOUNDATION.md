# Fundamentos
Existe un servicio web por parte del SAT el cual permite realizar consultas mediante [WSDL](https://es.wikipedia.org/wiki/WSDL)


## Proceso de solicitud
(Versión 1.2 del WS)

Al generar una nueva solicitud al WS esta pasa por varias etapas:
1. Solicitud de Descarga Masiva:
    - <https://www.sat.gob.mx/cs/Satellite?blobcol=urldata&blobkey=id&blobtable=MungoBlobs&blobwhere=1461175180762&ssbinary=true>
2. Verificación de Descarga de Solicitudes Exitosas:
    - <https://www.sat.gob.mx/cs/Satellite?blobcol=urldata&blobkey=id&blobtable=MungoBlobs&blobwhere=1461174995042&ssbinary=true>
3. Descarga de Solicitudes Exitosas:
    - <https://www.sat.gob.mx/cs/Satellite?blobcol=urldata&blobkey=id&blobtable=MungoBlobs&blobwhere=1461174995026&ssbinary=true>

Para cada una de estas fases es necesario primero obtener un token de autenticación.

### Autenticación
Para el proceso de autenticación se requiere enviar la FIEL (ver <https://gitlab.com/pycfdi/credentials/-/blob/main/doc/fiel_csd.md>)

### Apendices

- Endpoints reales:
  - <https://www.sat.gob.mx/cs/Satellite?blobcol=urldata&blobkey=id&blobtable=MungoBlobs&blobwhere=1461174995058&ssbinary=true>

La fuente de estos fundamentos es: <https://www.sat.gob.mx/consultas/42968/consulta-y-recuperacion-de-comprobantes-(nuevo)>
