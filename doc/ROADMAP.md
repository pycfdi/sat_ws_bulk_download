# Roadmap

## Técnico
- [ ] Definir mecanismo de comunicación con WS
  - [ ] Probar `zeep` <https://docs.python-zeep.org>
  - [ ] Probar peticiones `request` estandar
    - [ ] Analizar uso de `jinja` para formar los XML's
  - [ ] Analizar pros y contras

## Funcionalidades

Implementar los servicios del WS 1.2 del SAT
- [ ] Autenticación
  - [ ] Almacenar token para re-utilizarlo
- [ ] Envio de nuevas peticiones
- [ ] Revisión de estatus de petición
- [ ] Descarga de paquetes
- [ ] Habilitar funcionamiento en modo CLI
